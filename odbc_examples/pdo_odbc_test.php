<?php

require "../vendor/autoload.php";

$db_options = require "db_settings.php";

$pdo = new PDO($db_options['dsn'], $db_options['user'], $db_options['pass']);

$sql = "select * from scgenlck";
$stmt = $pdo->prepare($sql);
if (!$stmt) {
    echo implode($pdo->errorInfo()) . PHP_EOL;
    die("unable to prepare SQL: $sql");
}

if (!$stmt->execute()) {
    echo $stmt->errorInfo() . PHP_EOL;
    die("unable to execute sql");
}

while ($row = $stmt->fetch()) {
    echo json_encode($row) . PHP_EOL;
}
