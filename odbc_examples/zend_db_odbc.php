<?php

require "../vendor/autoload.php";

$db_options = require "db_settings.php";

$db = new \Zend\Db\Adapter\Adapter($db_options);

$tgScGenLck = new \Zend\Db\TableGateway\TableGateway('SCGENLCK', $db);

$rowset = $tgScGenLck->select();

$count = 0;
foreach ($rowset as $row) {
    $count++;
    echo json_encode($row) . PHP_EOL;
}

echo PHP_EOL . "found " . $count . " row(s)" . PHP_EOL;
